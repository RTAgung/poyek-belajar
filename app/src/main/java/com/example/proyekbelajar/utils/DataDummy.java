package com.example.proyekbelajar.utils;

import com.example.proyekbelajar.data.Movie;

import java.util.ArrayList;

public class DataDummy {
    // mengisi list data sementara
    public static ArrayList<Movie> getDummyMovie() {
        ArrayList<Movie> listMovie = new ArrayList<>();

        listMovie.add(
                new Movie(
                        "590706",
                        "Jiu Jitsu",
                        "Jiu Jitsu",
                        "Every six years, an ancient order of jiu-jitsu fighters joins forces to battle a vicious race of alien invaders. But when a celebrated war hero goes down in defeat, the fate of the planet and mankind hangs in the balance.",
                        "2020-11-20",
                        "/jeAQdDX9nguP6YOX6QSWKDPkbBo.jpg",
                        "/eLT8Cu357VOwBVTitkmlDEg32Fs.jpg"
                )
        );
        listMovie.add(
                new Movie(
                        "602211",
                        "Fatman",
                        "Fatman",
                        "A rowdy, unorthodox Santa Claus is fighting to save his declining business. Meanwhile, Billy, a neglected and precocious 12 year old, hires a hit man to kill Santa after receiving a lump of coal in his stocking.",
                        "2020-11-13",
                        "/ckfwfLkl0CkafTasoRw5FILhZAS.jpg",
                        "/4n8QNNdk4BOX9Dslfbz5Dy6j1HK.jpg"
                )
        );
        listMovie.add(
                new Movie(
                        "671583",
                        "Upside-Down Magic",
                        "Upside-Down Magic",
                        "Nory and her best friend Reina enter the Sage Academy for Magical Studies, where Nory’s unconventional powers land her in a class for those with wonky, or “upside-down,” magic. Undaunted, Nory sets out to prove that that upside-down magic can be just as powerful as right-side-up.",
                        "2020-07-31",
                        "/vam9VHLZl8tqNwkp1zjEAxIOrkk.jpg",
                        "/h9vTJ78h0SASYUxg4jV0AQ00oF2.jpg"
                )
        );
        listMovie.add(
                new Movie(
                        "590706",
                        "Jiu Jitsu",
                        "Jiu Jitsu",
                        "Every six years, an ancient order of jiu-jitsu fighters joins forces to battle a vicious race of alien invaders. But when a celebrated war hero goes down in defeat, the fate of the planet and mankind hangs in the balance.",
                        "2020-11-20",
                        "/jeAQdDX9nguP6YOX6QSWKDPkbBo.jpg",
                        "/eLT8Cu357VOwBVTitkmlDEg32Fs.jpg"
                )
        );
        listMovie.add(
                new Movie(
                        "602211",
                        "Fatman",
                        "Fatman",
                        "A rowdy, unorthodox Santa Claus is fighting to save his declining business. Meanwhile, Billy, a neglected and precocious 12 year old, hires a hit man to kill Santa after receiving a lump of coal in his stocking.",
                        "2020-11-13",
                        "/ckfwfLkl0CkafTasoRw5FILhZAS.jpg",
                        "/4n8QNNdk4BOX9Dslfbz5Dy6j1HK.jpg"
                )
        );
        listMovie.add(
                new Movie(
                        "671583",
                        "Upside-Down Magic",
                        "Upside-Down Magic",
                        "Nory and her best friend Reina enter the Sage Academy for Magical Studies, where Nory’s unconventional powers land her in a class for those with wonky, or “upside-down,” magic. Undaunted, Nory sets out to prove that that upside-down magic can be just as powerful as right-side-up.",
                        "2020-07-31",
                        "/vam9VHLZl8tqNwkp1zjEAxIOrkk.jpg",
                        "/h9vTJ78h0SASYUxg4jV0AQ00oF2.jpg"
                )
        );

        return listMovie;
    }

    // mengambil detail data dari salah satu item list data
    public static Movie getDummyDetailMovie(String id) {
        ArrayList<Movie> listMovie = getDummyMovie();
        Movie movie = null;

        for (int i = 0; i < listMovie.size(); i++) {
            if (listMovie.get(i).getId().equals(id)) {
                movie = listMovie.get(i);
            }
        }

        return movie;
    }
}
