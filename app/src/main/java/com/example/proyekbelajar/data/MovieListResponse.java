package com.example.proyekbelajar.data;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

// membuat kelas untuk menerima nested data object API
public class MovieListResponse {
    // SerializedName digunakan untuk menyamakan nama variabel di API dengan yang dibuat
    @SerializedName("results")
    private ArrayList<Movie> results;

    public MovieListResponse(ArrayList<Movie> results) {
        this.results = results;
    }

    public ArrayList<Movie> getResults() {
        return results;
    }
}
