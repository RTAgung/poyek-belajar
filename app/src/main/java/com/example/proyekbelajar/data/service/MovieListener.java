package com.example.proyekbelajar.data.service;

// membuat interface sebagai callback / jembatan
// yang mengantarkan data dari pengambilan data (MovieApi) kepada menampilkan data (Activity)
public interface MovieListener<T> {
    void onSuccess(T body);

    void onFailed(String message);
}