package com.example.proyekbelajar.data.service;

import com.example.proyekbelajar.data.Movie;
import com.example.proyekbelajar.data.MovieListResponse;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MovieApi {
    // membuat variabel untuk menampung URL dari API yang digunakan
    public static final String BASE_URL = "https://api.themoviedb.org/";

    // inisialisasi Retrofit sebagai library untuk mengakses API
    private Retrofit retrofit = null;

    // fungsi untuk membuat retrofit dan mengkoneksikan dengan request yang ada di MovieApiInterface
    MovieApiInterface getMovieApi() {
        if (retrofit == null) {
            // membangun atau instansiasi data retrofit
            retrofit = new Retrofit
                    .Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit.create(MovieApiInterface.class);
    }

    // mengambil list movie dari API
    public void getListMovie(MovieListener<ArrayList<Movie>> listener) {
        // eksekusi pemanggilan request ke API
        getMovieApi().getListMovie().enqueue(new Callback<MovieListResponse>() {
            // ketika berhasil mengambil data
            @Override
            public void onResponse(Call<MovieListResponse> call, Response<MovieListResponse> response) {
                try {
                    // menyimpan hasil pengambilan data
                    MovieListResponse movieResponse = response.body();
                    if (movieResponse != null) {
                        // menyimpan list data dari nested data yang berasal dari API
                        ArrayList<Movie> listMovie = movieResponse.getResults();
                        // mengirimkan data melalui Callback / jembatan listener
                        listener.onSuccess(listMovie);
                    }
                } catch (Exception e) {
                    // mengirimkan pesan error melalui Callback / jembatan listener
                    listener.onFailed(e.getMessage());
                }
            }

            // ketika gagal atau error pada saat mengambil data
            @Override
            public void onFailure(Call<MovieListResponse> call, Throwable t) {
                // mengirimkan pesan error melalui Callback / jembatan listener
                listener.onFailed(t.getMessage());
            }
        });
    }

    // mengambil detail movie dari API berdasarkan id
    public void getDetailMovie(MovieListener<Movie> listener, String id) {
        // eksekusi pemanggilan request ke API
        getMovieApi().getDetailMovie(id).enqueue(new Callback<Movie>() {
            // ketika berhasil mengambil data
            @Override
            public void onResponse(Call<Movie> call, Response<Movie> response) {
                try {
                    // menyimpan hasil pengambilan data
                    Movie movie = response.body();
                    if (movie != null) {
                        // mengirimkan data melalui Callback / jembatan listener
                        listener.onSuccess(movie);
                    }
                } catch (Exception e) {
                    // mengirimkan pesan error melalui Callback / jembatan listener
                    listener.onFailed(e.getMessage());
                }
            }

            // ketika gagal atau error pada saat mengambil data
            @Override
            public void onFailure(Call<Movie> call, Throwable t) {
                // mengirimkan pesan error melalui Callback / jembatan listener
                listener.onFailed(t.getMessage());
            }
        });
    }
}
