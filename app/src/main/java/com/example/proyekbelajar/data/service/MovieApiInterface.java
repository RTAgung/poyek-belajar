package com.example.proyekbelajar.data.service;

import com.example.proyekbelajar.data.Movie;
import com.example.proyekbelajar.data.MovieListResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface MovieApiInterface {

    // request ambil data dari API dengan link tertentu
    // GET sebagai metode akses yaitu mengambil data
    // Call untuk memanggil object untuk menampung data yang diambil
    // Path untuk membuat link menjadi dinamis
    @GET("/3/movie/popular?api_key=eeb7ac3deb1c5ee1471cdd22da14ed1a")
    Call<MovieListResponse> getListMovie();

    @GET("/3/movie/{movie_id}?api_key=eeb7ac3deb1c5ee1471cdd22da14ed1a")
    Call<Movie> getDetailMovie(@Path("movie_id") String id);
}