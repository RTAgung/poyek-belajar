package com.example.proyekbelajar.ui.adapter;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.proyekbelajar.R;
import com.example.proyekbelajar.data.Movie;
import com.example.proyekbelajar.ui.DetailActivity;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import static com.example.proyekbelajar.ui.DetailActivity.EXTRA_ID;

// membuat kelas adapter sebagai penghubung antara list dan item nya
public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.ViewHolder> {
    // deklarasi list yang akan ditampilkan
    private ArrayList<Movie> movies = new ArrayList<>();

    // mengisi data list yang akan di tampilkan
    public void setMovies(ArrayList<Movie> movies) {
        if (movies != null) {
            this.movies.clear();
            this.movies.addAll(movies);
        }
        // update tampilan jika terdapat perubahan data
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // mengkoneksikan layout item kedalam kelas adapter ini
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.movie_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        // mengirimkan data tiap satuan item ke kelas ViewHolder untuk ditampilkan
        holder.bind(movies.get(position));
    }

    // mengambil panjang list yang ada
    @Override
    public int getItemCount() {
        return movies.size();
    }

    // membuat kelas sebagai kontrol item di dalam view (movie_item.xml) agar dapat dimanipulasi
    public class ViewHolder extends RecyclerView.ViewHolder {
        // membuat variabel konstanta untuk melengkapi link yang kurang
        private static final String IMAGE_URL = "https://image.tmdb.org/t/p/w500/";

        // deklarasi variabel view sesuai dengan layout xml
        private TextView tvTitle;
        private ImageView ivImage;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            // koneksikan variabel dengan view berdasarkan id
            tvTitle = itemView.findViewById(R.id.tv_item_title);
            ivImage = itemView.findViewById(R.id.iv_item_image);
        }

        public void bind(Movie movie) {
            // menampilkan text ke dalam view item
            tvTitle.setText(movie.getTitle());
            // mengambil dan menampilkan image berdasarkan link ke dalam view item
            Glide.with(itemView.getContext())
                    .load(IMAGE_URL + movie.getBackdropPath())
                    .into(ivImage);

            // melakukan pindah halaman ke halaman detail
            itemView.setOnClickListener(v -> {
                Intent intent = new Intent(itemView.getContext(), DetailActivity.class);
                // mengirim data id ke detail dengan key tertentu (EXTRA_ID)
                intent.putExtra(EXTRA_ID, movie.getId());
                // memulai pindah halaman dengan bantuan context dari itemView
                itemView.getContext().startActivity(intent);
            });
        }
    }
}