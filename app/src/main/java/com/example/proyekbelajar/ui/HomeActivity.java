package com.example.proyekbelajar.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.example.proyekbelajar.R;
import com.example.proyekbelajar.data.Movie;
import com.example.proyekbelajar.data.service.MovieApi;
import com.example.proyekbelajar.data.service.MovieListener;
import com.example.proyekbelajar.ui.adapter.MovieAdapter;
import com.example.proyekbelajar.utils.DataDummy;

import java.util.ArrayList;

public class HomeActivity extends AppCompatActivity {

    // deklarasi variabel view sesuai dengan layout xml
    private TextView welcome;
    private RecyclerView rvMovie;

    // deklarasi adapter yang telah dibuat
    private MovieAdapter movieAdapter;

    // inisialisasi Callback
    private MovieListener<ArrayList<Movie>> listMovieListener = new MovieListener<ArrayList<Movie>>() {
        // menerima data ketika berhasil
        @Override
        public void onSuccess(ArrayList<Movie> body) {
            // memasukkan data ke dalam list
            movieAdapter.setMovies(body);
        }

        // menerima data ketika gagal
        @Override
        public void onFailed(String message) {
            // menampilkan notif message
            Toast.makeText(HomeActivity.this, message, Toast.LENGTH_SHORT).show();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        // mengganti tulisan di action bar (paling atas)
        getSupportActionBar().setTitle("Home");

        // koneksikan variabel dengan view berdasarkan id
        welcome = findViewById(R.id.tv_welcome);
        rvMovie = findViewById(R.id.rv_movie_list);

        // menerima kiriman data dari halaman sebelumnya menggunakan kuncinya (EXTRA_USERNAME)
        String username = getIntent().getStringExtra(MainActivity.EXTRA_USERNAME);

        // menampilkan text ke dalam view
        welcome.setText("Welcome, " + username);

        // menyambungkan adapter yang telah dibuat ke dalam variabel
        movieAdapter = new MovieAdapter();
        // memberi jenis leyout yang akan ditampilkan ke view
        rvMovie.setLayoutManager(new LinearLayoutManager(this));
        // memasukkan adapter ke dalam view
        rvMovie.setAdapter(movieAdapter);

        // inisialisasi API
        MovieApi movieApi = new MovieApi();
        // memanggil method getListMovie untuk request data API
        // dengan menggunakan Callback / jembatan listMovieListener
        movieApi.getListMovie(listMovieListener);
    }

    // memunculkan tombol yang ada di action bar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // menghubungkan tombol-tombol dengan menu yang telah dibuat sebelumnya
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return super.onCreateOptionsMenu(menu);
    }

    // membuat aksi pada tombol yang ada di action bar (paling atas)
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        // identifikasi tombol mana yang diklik berdasarkan id
        switch (item.getItemId()) {
            // tombol profile
            case R.id.action_profile:
                // deklarasi intent seperti biasa
                Intent intent = new Intent(this, ProfileActivity.class);
                // memulai pindah halaman
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}